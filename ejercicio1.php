
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
              rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">        
    </head>
    <body>

        <?php
        include './menu.php';

        if (isset($_GET["boton"])) {
            $nombre = $_GET["nombre"];
            $apellido = $_GET["apellido"];
            ?>
            <div class="container-fluid">
                <div class="row" style="justify-content: center">
                    <div class="card col-2 text-center p-0 m-5">
                        <div class="card-header">
                            Nombre
                        </div>
                        <div class="card-body">                    
                            <p class="card-text"><?= $_GET["nombre"] ?></p>                    
                        </div>
                    </div>

                    <div class="card col-2 text-center p-0 m-5">
                        <div class="card-header">
                            Apellidos 
                        </div>
                        <div class="card-body">                    
                            <p class="card-text"><?= $_GET["apellido"] ?></p>                    
                        </div>
                    </div>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="container-fluid">
                <form>
                    <div class="mb-3 mt-3">
                        <label for="nombre" class="form-label">Nombre:</label>
                        <input type="text" class="form-control" id="nombre" placeholder="Introducir Nombre" name="nombre">
                    </div>
                    <div class="mb-3">
                        <label for="apellido" class="form-label">Apellidos:</label>
                        <input type="text" class="form-control" id="apellido" placeholder="Introducir Apellidos" name="apellido">
                    </div>                
                    <button name="boton" class="btn btn-primary">Enviar</button>
                </form>
            </div>
            <?php
        }
        ?>
    </body>
</html>