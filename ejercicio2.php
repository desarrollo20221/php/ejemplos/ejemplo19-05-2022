<?php
include './menu.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
              rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css"> 
        <title></title>
    </head>
    <body>        
        <?php
        if(isset($_GET["boton"])){
        ?>
            <div class="container-fluid">
                <div class="row" style="justify-content: center">
                    <div class="card col-3 text-center p-0 m-3">
                        <div class="card-header">
                            Email
                        </div>
                        <div class="card-body">                    
                            <p class="card-text"><?= $_GET["correo"] ?></p>                    
                        </div>
                    </div>
                    
                    <div class="card col-3 text-center p-0 m-3">
                        <div class="card-header">
                            Password
                        </div>
                        <div class="card-body">                    
                            <p class="card-text"><?= $_GET["contra"] ?></p>                    
                        </div>
                    </div>
                </div>
                <div class="row" style="justify-content: center">
                    <div class="card col-3 text-center p-0 m-3">
                        <div class="card-header">
                            Mes de acceso
                        </div>
                        <div class="card-body">                    
                            <p class="card-text"><?= $_GET["mes"] ?></p>                    
                        </div>
                    </div>
                    
                    <div class="card col-3 text-center p-0 m-3">
                        <div class="card-header">
                            Formas de acceso
                        </div>
                        <div class="card-body">                    
                            <?php
                                if(!isset($_GET["acceso"])){
                                    echo "<p class=\"card-text\">No has seleccionado ninguna forma de acceso</p>";
                                }else{
                                    foreach ($_GET["acceso"] as $value){
                                        echo "<p class =\"card-text\">$value</p>";
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
                
                <div class="row" style="justify-content: center">
                    <div class="card col-3 text-center p-0 m-3">
                        <div class="card-header">
                            Ciudad
                        </div>
                        <div class="card-body">                    
                            <p class="card-text"><?= $_GET["ciudad"] ?></p>                    
                        </div>
                    </div>
                    
                    <div class="card col-3 text-center p-0 m-3">
                        <div class="card-header">
                            Navegadores utilizados
                        </div>
                        <div class="card-body">                    
                            <?php
                                if (!isset($_GET["navegador"])) {
                                    echo "<p class=\"card-text\">no hay navegadores seleccionados</p>";
                                } else {
                                    foreach ($_GET["navegador"] as $valor) {
                                        echo "<p class=\"card-text\">$valor</p>";
                                    }
                                }
                                ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php    
        }else{
        ?>
        <form>
            <div class="container-fluid">
                <!-- PRIMER FORMULARIO -->
                <div class="row pt-3">
                    <div class="col-3">
                        <label for="correo" class="form-label">Nombre:</label>
                    </div>

                    <div class="col-9">
                        <input type="email" class="form-control" id="correo" placeholder="Email" name="correo">
                    </div>
                </div>
                <div class="row pt-3">
                    <div class="col-3">
                        <label for="contra" class="form-label">Apellidos:</label>
                    </div>

                    <div class="col-9">
                        <input type="password" class="form-control" id="contra" placeholder="contraseña" name="contra">
                    </div> 
                </div>
                <!-- FORMULARIO DE RADIO -->
                <div class="row pt-3">
                    <legend class="col-form-label col-3">Mes de acceso</legend>
                    <div class="col-9">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="mes" id="enero" value="enero" checked>
                            <label class="form-check-label" for="enero">
                                Enero
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="mes" id="febrero" value="febrero">
                            <label class="form-check-label" for="febrero">
                                Febrero
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="mes" id="marzo" value="marzo">
                            <label class="form-check-label" for="marzo">
                                Marzo
                            </label>
                        </div>
                    </div>
                </div>


                <!-- FORMULARIO DE CHECK -->             
                <div class="row pt-3">
                    <legend class="col-form-label col-3">Formas de acceso</legend>
                    <div class="col-9">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="acceso[]" id="tele" value="Telefono">
                            <label class="form-check-label" for="tele">
                                Telefono
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="acceso[]" id="orde" value="Ordenador">
                            <label class="form-check-label" for="orde">
                                Ordenador
                            </label>
                        </div>                       
                    </div>
                </div>
                <!-- FORMULARIO SELECT -->
                <div class="row pt-3">
                    <legend class="col-form-label col-3">Ciudad</legend>
                    <div class="col-9">
                        <select class="form-select" aria-label="Default select example" id="ciudad" name="ciudad" required>
                            <option value="" disabled selected>Seleccione una ciudad</option>
                            <option value="Santander">Santander</option>
                            <option value="Oviedo">Oviedo</option>
                            <option value="Bilbao">Bilbao</option>
                        </select>
                    </div>
                </div>

                <!-- FORMULARIO DE SELECCION MULTIPLE -->
                <div class="row pt-3">
                    <legend class="col-form-label col-3">Navegadores Utilizados</legend>
                    <div class="col-9">
                        <select class="form-select" multiple aria-label="multiple select example" id="navegador" name="navegador[]">
                            <option value="" disabled selected>Selecciona navegador</option>
                            <option value="Google">Google</option>
                            <option value="Edge">Edge</option>
                            <option value="Safari">Safari</option>
                        </select>
                    </div>
                </div>


                <button name="boton" class="btn btn-primary">Enviar</button>
            </div>
        </form>
        <?php
        }
        ?>
    </body>
</html>
