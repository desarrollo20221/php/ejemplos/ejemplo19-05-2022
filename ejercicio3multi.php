<?php
include './menu.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
              rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">        
    </head>
    <body>        
        <?php
        if (isset($_GET["boton"])) {
            $numeros = $_GET["nums"];
            $pro = 1;
            $resto = explode(";", $numeros[2]);
            unset($numeros[2]);
            $numeros = array_merge($numeros, $resto);
            foreach ($numeros as $numero) {
                $pro *= $numero;
            }
            ?>
            <div class="container-fluid">
                <div class="row" style="justify-content: center">
                    <div class="card col-2 text-center p-0 m-5">
                        <div class="card-header">
                            Producto
                        </div>
                        <div class="card-body">                    
                            <p class="card-text"><?= $pro ?></p>                    
                        </div>
                    </div>

                    <div class="card col-2 text-center p-0 m-5">
                        <div class="card-header">
                            Numeros Introducidos  
                        </div>
                        <div class="card-body">                    
                            <?php
                            foreach ($numeros as $valor) {
                                echo "<p class=\"card-text\">$valor</p>";
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>

            <?php
        } else {
            ?>
            <form>
                <div class="container-fluid">                
                    <div class="row pt-3">
                        <div class="col-3">
                            <label for="num1" class="form-label">Numero 1:</label>
                        </div>

                        <div class="col-9">
                            <input type="number" class="form-control" id="num1" placeholder="Introduce un numero" name="nums[]" required>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-3">
                            <label for="num2" class="form-label">Numero 2:</label>
                        </div>

                        <div class="col-9">
                            <input type="number" class="form-control" id="num2" placeholder="Introduce un numero" name="nums[]" required>
                        </div>
                    </div>

                    <div class="row pt-3">
                        <div class="col-3">
                            <label for="numeros" class="form-label">Numeros:</label>
                        </div>

                        <div class="col-9">
                            <input type="text" class="form-control" id="numeros" placeholder="Numeros separados por ;" name="nums[]" required>
                        </div>
                    </div>
                    <button name="boton" class="btn btn-primary">Multiplicar</button>
                </div>
            </form>
            <?php
        }
        ?>
    </body>
</html>
