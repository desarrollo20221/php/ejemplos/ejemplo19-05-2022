<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
              rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
              crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">        
    </head>
    <body>
        <?php
        include './menu.php';
        ?>
        <div class="container-fluid">
            <div class="row row-cols-1 row-cols-md-3 g-3 mt-3">
                <div class="col-md-3">
                    <div class="card h-100">
                        <img src="imgs/aguila.jpg" class="card-img-top" data-toggle="modal" data-target="#foto1">
                        <div class="card-body">
                            <h5 class="card-title">Foto 1</h5>
                            <p class="card-text">
                                Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit.
                                Morbi euismod felis non lorem rhoncus facilisis.
                                Phasellus ornare, elit et faucibus placerat,
                                turpis purus hendrerit lectus, quis placerat urna lacus a leo. Donec malesuada magna eget.
                            </p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">12 Enero 2022</small>
                        </div>
                    </div>
                </div>

                <div id="foto1" class="modal fade">
                    <div class="modal-dialog">
                        <div class="model-content">
                            <img src="imgs/aguila.jpg" class="modal-lg" data-dismiss="modal">
                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <div class="card h-100">
                        <img src="imgs/aguilas1.jpg" class="card-img-top" data-toggle="modal" data-target="#foto2">
                        <div class="card-body">
                            <h5 class="card-title">Foto 2</h5>
                            <p class="card-text">
                                Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit.
                                Morbi euismod felis non lorem rhoncus facilisis.
                                Phasellus ornare, elit et faucibus placerat,
                                turpis purus hendrerit lectus, quis placerat urna lacus a leo. Donec malesuada magna eget.
                            </p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">12 Enero 2022</small>
                        </div>
                    </div>
                </div>

                <div id="foto2" class="modal fade">
                    <div class="modal-dialog">
                        <div class="model-content">
                            <img src="imgs/aguilas1.jpg" class="modal-lg" data-dismiss="modal">
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card h-100">
                        <img src="imgs/aguilas2.jpg" class="card-img-top" data-toggle="modal" data-target="#foto3">
                        <div class="card-body">
                            <h5 class="card-title">Foto 3</h5>
                            <p class="card-text">
                                Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit.
                                Morbi euismod felis non lorem rhoncus facilisis.
                                Phasellus ornare, elit et faucibus placerat,
                                turpis purus hendrerit lectus, quis placerat urna lacus a leo. Donec malesuada magna eget.
                            </p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">12 Enero 2022</small>
                        </div>
                    </div>
                </div>

                <div id="foto3" class="modal fade">
                    <div class="modal-dialog">
                        <div class="model-content">
                            <img src="imgs/aguilas2.jpg" class="modal-lg" data-dismiss="modal">
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="card h-100">
                        <img src="imgs/aguilas3.jpg" class="card-img-top" data-toggle="modal" data-target="#foto4">
                        <div class="card-body">
                            <h5 class="card-title">Foto 4</h5>
                            <p class="card-text">
                                Lorem ipsum dolor sit amet,
                                consectetur adipiscing elit.
                                Morbi euismod felis non lorem rhoncus facilisis.
                                Phasellus ornare, elit et faucibus placerat,
                                turpis purus hendrerit lectus, quis placerat urna lacus a leo. Donec malesuada magna eget.
                            </p>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">12 Enero 2022</small>
                        </div>
                    </div>
                </div>

                <div id="foto4" class="modal fade">
                    <div class="modal-dialog">
                        <div class="model-content">
                            <img src="imgs/aguilas3.jpg" class="modal-lg" data-dismiss="modal">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    </body>
</html>
